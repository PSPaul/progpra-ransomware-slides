#!/usr/bin/env python


def en2ru(s):
    return s.encode('windows-1252').decode('windows-1251')


def main():
    try:
        while True:
            print(en2ru(input()))
    except KeyboardInterrupt:
        return


if __name__ == '__main__':
    main()
